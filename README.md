in the path folder

1. composer install
2. create .env file (just copy paste .env.example)
3. in your .env file modify your credentials in DB_DATABASE=yourdb
DB_USERNAME=root
4.php artisan key:generate
5. php artisan migrate
6. php artisan serve

# Reconstruct DB
php artisan migrate:rollback; composer dump-autoload;
php artisan migrate; php artisan db:seed;

# Creating new DB Controller
php artisan make:model Students
php artisan make:controller CONTROLLERNAME
php artisan make:migration create_students_table
