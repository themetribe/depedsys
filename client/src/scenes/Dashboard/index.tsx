import React from "react";
import {Sidebar} from "./../../components/Sidebar";
import { BrowserRouter as Router, Switch, Route, useRouteMatch} from "react-router-dom";
import { Grade } from "./Grade";
import {Section} from "./Section";
import {Students} from "./Students";
import {Subjects} from "./Subjects";
import {Reports} from "./Reports";
import {Profiles} from "./Profiles";
import { Settings } from "./Settings";
import { Lessons } from "./Subjects/Lessons";
import Lesson from "./Subjects/Lessons/lesson"; // need to adjust other component to be like lesson without {} / having export default
import Profile from "./Profiles/profile";

import "./style.scss"
import { Quizes } from "./Subjects/Lessons/Quizes";
import { ProtectedRoute } from "../../global/utils";

export const Dashboard = () => {
    let { path, url } = useRouteMatch();


    return (
        <>
            <div className="dashboard-container">
                <Sidebar />
                <div className="dashboard-content">
                    <ProtectedRoute path={`${url}/grade`} exact component={Grade}/>
                    <ProtectedRoute path={`${url}/sections`} exact component={Section} />
                    <ProtectedRoute path={`${url}/sections/:level_id`} component={Section} />
                    <ProtectedRoute path={`${url}/students`} exact component={Students} />
                    <ProtectedRoute path={`${url}/students/:section_id`} component={Students} />
                    
                    <ProtectedRoute path={`${url}/subjects`} component={Subjects} />
                    {/* ===== */}
                    <ProtectedRoute path={`${url}/lessons`} exact component={Lessons} />
                    <ProtectedRoute path={`${url}/lessons/:subject_id`} exact component={Lessons} />
                    <ProtectedRoute path={`${url}/lesson/:id`} component={Lesson} />
                    <ProtectedRoute path={`${url}/quizes/:lesson_id`} component={Quizes} />
                    {/* ===== */}
                    <ProtectedRoute path={`${url}/reports`} component={Reports} />
                    <ProtectedRoute path={`${url}/settings`} component={Settings} />
                    <ProtectedRoute path={`${url}/profile`} exact component={Profiles} />
                    <ProtectedRoute path={`${url}/profile/:id`} component={Profile} />
                </div>
            </div>
            
        
        </>
    );
};
