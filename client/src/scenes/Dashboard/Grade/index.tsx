import React, { useEffect, useState } from "react";
import { DataGrid } from "../../../components/DataGrid";
import { API_URL } from "../../../config";
import _ from "lodash";

export const Grade = () => {
    const dataMap = [{
        key: "id",
        isReadOnly: true,
        type: "field"
    },
    {
        key: "name",
        isReadOnly: true,
        type: "field"
    },
    {
        key: "img",
        isReadOnly: true,
        type: "field"
    }]
    const [state, setState] = useState<any>({});

    const getData = async () => {
        const levels = await fetch(API_URL + "levels", {
            headers: { "Content-Type": "application/json" },
            method: "GET",
        }).then((res) => res.json());
        levels && setState({ data: levels });
    };
    useEffect(() => {
        getData();
    }, []);
    const invokeRows = (data: any) => {
        return (
            data &&
            data.map((v: any) => [ // can be refactored from dataMap
                {
                    key: "id",
                    isReadOnly: true,
                    type: "field",
                    val: v.id,
                },
                {
                    key: "name",
                    isReadOnly: true,
                    type: "field",
                    val: v.name,
                },
                {
                    key: "img",
                    isReadOnly: true,
                    type: "field",
                    val: v.img,
                },
                { key: "action", type: ["c", "r", "u", "d"] },
            ])
        );
    };
    const updateItem = (args: any) => {
        const updatedData = state.data && state.data.map((item: any) => {
            if (item.id === args.id) {
                item = {...item, ...args}
            }
            return item;
        });
        setState({
            ...state,
            data: updatedData, // 'level', 'name', 'date' is a reserved word, so please don't use in db and in row key
        });
    };
    const saveUpdate = (args: any) => {
        fetch(API_URL + `level/` + args.id, {
            headers: { "Content-Type": "application/json" },
            method: "PUT",
            body: JSON.stringify(args),
        }).then((res) => res.json()).
        then((res) => {
            alert('Data saved!')
        })
    }
    const addItem = (args: any) => {
        console.log({args})
        let url = API_URL + "level";
        fetch(url, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(args),
        }).then((result) => {
            result.json().then((resp) => {
                alert("Data Added Successfuly");
                const newData = _.cloneDeep(state.data);
                newData.push(args);
                setState({ ...state, data: newData });
            });
        });
    };
    const deleteItem = async (id: any) => {
        if(window.confirm("You want to delete this data?")) {
            const sections = await fetch(API_URL + `level/${id}`, {
                method: "DELETE",
            })
            .then(res => res.json())
            .then(res => {
                const newData = state.data.filter((i: any) => i.id !== id);
                setState({ ...state, data: newData });
            })            
        }
    };

    return (
        <>
            <h1>Grade Level</h1>
            <DataGrid 
                hasNew={true}
                defaultData={dataMap}
                datas={invokeRows(state.data || [])} 
                actions={{
                    c:addItem,
                    r:'/dashboard/sections',
                    u:updateItem,
                    d:deleteItem
                }}
                saveUpdate={saveUpdate}
            />
        </>
    );
};
