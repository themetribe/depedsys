import React, { useState, useEffect } from "react";
import { Button, Accordion, AccordionSummary, Typography, AccordionDetails } from "@material-ui/core";
import PostAddIcon from '@material-ui/icons/PostAdd';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SunEditor from "suneditor-react";
import { assign } from "lodash";

export const Assignments = (props : any) => {
    
    return (
        <>
            <Button
                variant="contained"
                color="primary"
                size="large"
                endIcon={<PostAddIcon />}
                onClick={async () => {
                    const _assignment = [...props.assignment]
                    _assignment.push({id: -1, description: ''})
                    props.setAssignment(_assignment)
                }}
            >
                Add Assignment
            </Button><br /><br />
            {props.assignment?.length > 0 && props.assignment?.map((item:any, i:any) => (
                <Accordion key={i}>
                    <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                    >
                    <Typography>Assignment # {i+1}</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                    <SunEditor
                    setContents={item.description || ''}
                    onChange={(value) => {
                        const _assignment = props.assignment.map((_item:any, _i:any) => {
                            if(_i===i) {
                                _item.description = value
                            }
                            return _item
                        })
                        props.setAssignment(_assignment)
                    }}
                    setOptions={{
                    "minHeight": "100px",
                    "buttonList": [
                        [   
                            "blockquote",
                            "bold",
                            "underline",
                            "italic",
                            "fontColor",
                            "list",
                            "link",
                            "image"
                        ]
                    ]
                }}  />
                    </AccordionDetails>
                </Accordion>
            ))}
            
        </>
    );
};
