import React, {useState, useEffect} from "react";
import { DataTable } from "../../../../components/DataTable";
import Lesson from "./lesson";
import { useParams } from "react-router-dom";
import { API_URL } from "../../../../config";

export const Lessons = () => {
    const {subject_id} = useParams()
    const header = ["#", "Title", "Subject", "Date", "Action"]
    const [state, setState] = useState<any>({})
    const [subjects, setSubjects] = useState<any>({})
    
    const getData = async () => {
        const lessons = await fetch([API_URL + "lessons", subject_id || ''].join('/'), {
            headers: { "Content-Type": "application/json" },
            method: "GET",
        }).then((res) => res.json())
        .then((res) => ( 
            res.map((i:any) => ({
                id : i.id,
                title : i.title,
                created_at : i.created_at,
                subject : {
                    value: i.subject_id,
                    label: i.subject.name
                }
            }))
        ))
        lessons && setState({ data: lessons });
    }
    // This can be improved in the future by loading at first after login.
    const getSubjects = async () => {
        const subjects = await fetch(API_URL + "subjects", {
            headers: { "Content-Type": "application/json" },
            method: "GET",
        }).then((res) => res.json())
        .then((res) => res.map((i:any) => ({
            value: i.id,
            label : i.name
        })));
        subjects && setSubjects(subjects)
    }
    
    useEffect(() => {
        getData()
        getSubjects()
    },[])
    const invokeRows = (data:any) => {
        return (
            state.data &&
            state.data.map((v: any, i:Number) => [
                { key: "id", isReadOnly:true, type: "field", val: v.id },  
                { key: "title", isReadOnly:true, type: "field", val: v.title },
                { key: "subject", isReadOnly:true, type: "select", val: v.subject, options: subjects },
                { key: "date", isReadOnly:true, type: "date", val: v.created_at },
                { key: "action", type: ['r', 'u', 'd'] },
            ])
          )
    }
    const deleteFunc = (id : any) => {
        alert('Put the delete api function here, deleting the ID : #' + id)
        // once deleted, then remove that item from the array state.
        const newData = state.data.filter((i:any) => i.id!==id)
        console.log({newData})
        setState({...state, data:newData})
    }
    
    const updateItem = (args: any) => {
        const updatedData = state.data && state.data.map((item: any) => {
            if (item.id === args.id) {
                item = {...item, ...args}
            }
            return item;
        });
        setState({
            ...state,
            data: updatedData, // 'level', 'name', 'date' is a reserved word, so please don't use in db and in row key
        });
    };

    const saveUpdate = (args: any) => {
        fetch(API_URL + `lesson/` + args.id, {
            headers: { "Content-Type": "application/json" },
            method: "PUT",
            body: JSON.stringify(args),
        }).then((res) => res.json()).
        then((res) => {
            alert('Data saved!')
        })
    }

    

    return (
        <>
            <h1>Lessons</h1>
            <DataTable header={header} hasNew={true} rows={invokeRows(state.data)} 
                actions={{
                    r:"/dashboard/lesson",
                    u:updateItem,
                    d:deleteFunc
                }} 
                saveUpdate={saveUpdate}
                />
        </>
    );
};
