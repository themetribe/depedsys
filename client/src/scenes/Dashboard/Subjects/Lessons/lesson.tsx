import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';
import SunEditor from 'suneditor-react';
import 'suneditor/dist/css/suneditor.min.css'; // Import Sun Editor's CSS File
import { TextField, FormControl, Button } from '@material-ui/core';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import SaveIcon from '@material-ui/icons/Save';
import { Assignments } from './Assignments';
import { Link, useParams } from 'react-router-dom';
import { API_URL } from '../../../../config';
import "./style.scss"

const Lesson = (_props:any) => {
    const {id} = useParams()
    const [title, setTitle] = useState<any>('default title');
    const [meetingUrl, setMeetingUrl] = useState<any>('');
    const [content, setContent] = useState<any>('');
    const [assignments, setAssignments] = useState<any>([])

    const fetchData = useCallback(() => {
        fetch(API_URL + "lesson/" + id)
            .then(res => res.json())
            .then((res) => {
                setTitle(res.title)
                setContent(res.content || '')
                setMeetingUrl(res.meetingUrl)
                setAssignments(res.assignments)
            })
            .catch((error) => {
                console.log(error)
            })
    }, [])
    
    useEffect(() => {
        fetchData()
    }, [])

    const save = () => {
        fetch(API_URL + "lesson/"+id, {
            method: "PUT",
            body : JSON.stringify({
                title,
                content,
                meetingUrl,
                assignments
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
        .then((res) => {
            alert('Saved')
        })
    }
    return (
        <>
            <div className="flex-apart">
                <h2>Lesson</h2>
                <Button
                    onClick={save}
                        variant="contained"
                        color="primary"
                        size="large"
                        endIcon={<SaveIcon />}
                    >
                        Publish
                </Button>
            </div>
            <div className="form-fields">
                <FormControl component="fieldset">
                    <TextField
                        id="outlined-basic"
                        label="Lesson Title"
                        variant="outlined"
                        value={title}
                        onChange={(e:React.ChangeEvent<{ value: any }>) => {
                            setTitle(e.target.value)
                        }}
                    />
                </FormControl>
            </div>
            <hr />
            <h5>Lesson Body </h5>

            <SunEditor
            setContents={content}
            onChange={(value) => {
                setContent(value)
            }}
            setOptions={{
                "minHeight": "400px",
                "buttonList": [
                    [
                        "undo",
                        "redo",
                        "font",
                        "fontSize",
                        "formatBlock",
                        "blockquote",
                        "bold",
                        "underline",
                        "italic",
                        "fontColor",
                        "hiliteColor",
                        "horizontalRule",
                        "list",
                        "table",
                        "link",
                        "image",
                        "video",
                        "fullScreen",
                        "preview",
                        "print",
                    ]
                ]
            }}  />
            <div className="form-fields">
                <FormControl component="fieldset">
                    <h4>Meeting Link </h4>
                    <TextField
                        id="outlined-basic"
                        label="(e.g. Zoom, Google Meet/Hangouts"
                        variant="outlined"
                        value={meetingUrl}
                        onChange={(e:React.ChangeEvent<{ value: any }>) => {
                            setMeetingUrl(e.target.value)
                        }}
                    />
                </FormControl>
            </div>
            <hr />
            <h3>Discussion Board</h3>
            <p>Under construction - This section will enable students and teachers to make comments or discuss anything about the topic. This will look like a traditional blog comment section which is heirarchical.</p>
            <hr />
            <Assignments setAssignment={setAssignments} assignment={assignments} />
            <Link to={["/dashboard/quizes", id].join('/')}>
                <Button
                    variant="contained"
                    color="default"
                    size="large"
                    endIcon={<PlaylistAddCheckIcon />}
                >
                    Add Quiz
                </Button>
            </Link>
            
        </>
    );
};

Lesson.propTypes = {
    
};

export default Lesson;