import React, { useState, useCallback, useEffect } from "react";
import { Button, Accordion, AccordionSummary, Typography, AccordionDetails, TableContainer, Paper, Table, TableBody, TableRow, TableCell, TextField, FormControl, InputLabel, Select, MenuItem, IconButton, FormControlLabel, Switch } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SunEditor from "suneditor-react";
import { useParams } from "react-router-dom";
import { API_URL } from "../../../../../config";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import "./style.scss"

import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

export const Quizes = () => {
    const {lesson_id} = useParams()
    const [quiz, setQuiz] = useState<any>([])

    const save = () => {
        const dataToSave = quiz.map((i:any)=> {
            const {description, choices, answer} = i;
            return ({
                description,
                choices : choices.join('|'),
                answer,
                lesson_id
            })
        })
        // console.log({dataToSave})
        fetch(API_URL + "quiz/"+lesson_id, {
            method: "PUT",
            body : JSON.stringify(dataToSave),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
        .then((res) => {
            alert('Saved')
        })
    }
    const fetchData = useCallback(() => {
        fetch(API_URL + "quiz/" + lesson_id)
            .then(res => res.json())
            .then((_res:any) => {
                const res = _res.map((i:any) => {
                    const {description, answer, choices, id, duedate} = i
                    return ({
                        description,
                        answer,
                        choices : choices.split('|'),
                        id,
                        duedate
                    })
                })
                setQuiz(res)
            })
            .catch((error) => {
                console.log(error)
            })
    }, [])
    
    useEffect(() => {
        fetchData()
    }, [])
    
    return (
        <div className="quiz-builder-container">
            <h3>The Quiz Builder</h3>
            <hr />
            <div className="settings-container">
                <div className="settings">
                    <div className="form-fields">
                        <FormControl component="fieldset">
                            <TextField
                                id="outlined-basic"
                                label={"Number of items to generate"}
                                type="number"
                                variant="outlined"
                                onChange={(e:React.ChangeEvent<{ value: any }>) => {
                                    console.log('under construction...')
                                }}
                            />
                        </FormControl>
                    </div>
                    <div className="form-fields">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            margin="normal"
                            id="date-picker-dialog"
                            label="Schedule Available"
                            format="MMMM d, yyyy"
                            value={(Date.now())}
                            onChange={() => {
                                console.log('under construction')
                            }}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </MuiPickersUtilsProvider>
                    </div>
                    <div className="form-fields">
                        <FormControlLabel
                            control={<Switch checked={true} onChange={() => {
                                console.log('under construction ')
                            }} />}
                            label="Scramble Questions"
                            labelPlacement="start"
                        />
                    </div>

                </div>
                <div className="buttons">
                    <Button
                        variant="contained"
                        color="secondary"
                        size="large"
                        onClick={save}>
                        Generate Preview
                    </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        size="large"
                        onClick={save}>
                        Set Quiz
                    </Button>
                </div>
            </div>
            <ol className="questions-container">
            {quiz.length > 0 && quiz.map((item:any, i:any) => (
                <li className="item" id={['li-item-',i].join('')}>
                    <span>{(i+1)})</span>
                    <div className="question-container">
                        <SunEditor 
                            setContents={item.description || ''}
                            onChange={(value) => {
                                item.description = value
                            }}
                            setOptions={{
                            "minHeight": "100px",
                            "buttonList": [
                                [   
                                    "blockquote",
                                    "bold",
                                    "underline",
                                    "italic",
                                    "fontColor",
                                    "list",
                                    "link",
                                    "image"
                                ]
                            ]
                        }}  />
                        <div>
                            <strong>Choices </strong>
                            <IconButton
                                size="medium"
                                edge="start"
                                classes={{ root: "btnNew" }}
                                onClick={() => {
                                    const _quiz = [...quiz]
                                    _quiz[i].choices.push('')
                                    setQuiz(_quiz)
                                }}
                            >
                                <AddCircleIcon fontSize="large" />
                            </IconButton>
                            <span className="log-note">Note: Without choices will be considered as essay.</span>
                        </div>
                        <br />
                        <div className="choices">
                            {item.choices && item.choices.map((choice:any, _i:any) => (
                                <div className="choice">
                                    <FormControl component="fieldset">
                                        <TextField
                                            id="outlined-basic"
                                            label={"Option " + (i+1)}
                                            variant="outlined"
                                            value={choice}
                                            onChange={(e:React.ChangeEvent<{ value: any }>) => {
                                                const _quiz = [...quiz]
                                                _quiz[i].choices[_i] = e.target.value
                                                setQuiz(_quiz)
                                            }}
                                        />
                                    </FormControl>
                                </div>
                            ))}
                        </div>
                        <br />
                        <div>
                            <strong>Correct Answer </strong>
                            <div className="form-fields">
                                <FormControl variant="filled">
                                    <InputLabel id="demo-simple-select-filled-label">Please select the correct answer</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-filled-label"
                                        id="demo-simple-select-filled"
                                        value={item.answer}
                                        onChange={(e:React.ChangeEvent<{ value: any }>) => {
                                            const _quiz = [...quiz]
                                            _quiz[i].answer = e.target.value
                                            setQuiz(_quiz)
                                        }}
                                        >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        {item.choices && item.choices.map((choice:any, i:any) => (
                                            <MenuItem value={choice}>{choice}</MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </div>
                        </div>
                    </div>
                </li>
            ))}
            </ol>

            <div className="add-question-block">
                
                <IconButton
                    size="medium"
                    edge="start"
                    classes={{ root: "btnNew" }}
                    onClick={() => {
                        const _quiz = [...quiz]
                        _quiz.push({id:quiz.length + 1, description : '', choices: [], answer: ''})
                        setQuiz(_quiz)
                        window.location.href=['#','li-item-',quiz.length].join('');
                    }}
                >
                    <AddCircleIcon fontSize="large" />
                </IconButton>
                <strong>Add Question</strong>
            </div>

        </div>
    );
};
