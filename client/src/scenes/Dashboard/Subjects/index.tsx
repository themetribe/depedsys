import React, { useEffect, useState } from "react";
import { DataTable } from "../../../components/DataTable";
import _ from "lodash";
import { API_URL } from "../../../config";

export const Subjects = () => {
    const header = ["#", "Subject Name", "Grade Level", "Action"];
    const [state, setState] = useState<any>({});
    const [levels, setLevels] = useState<any>([]);

    const getData = async () => {
        const subjects = await fetch(API_URL + "subjects", {
            headers: { "Content-Type": "application/json" },
            method: "GET",
        }).then((res) => res.json())
        .then((res) => ( 
            res.map((i:any) => ({
                id : i.id,
                name : i.name,
                level : {
                    value: i.level_id,
                    label: i.level.name
                }
            }))
        ))
        console.log({subjects})
        subjects && setState({ data: subjects });
    };
    // This can be improved in the future by loading at first after login.
    const getGradeLevel = async () => {
        const levels = await fetch(API_URL + "levels", {
            headers: { "Content-Type": "application/json" },
            method: "GET",
        }).then((res) => res.json())
        .then((res) => res.map((i:any) => ({
            value: i.id,
            label : i.name
        })));
        levels && setLevels(levels)
    }

    useEffect(() => {
        getData();
        getGradeLevel();
    }, []);

    const invokeRows = (data: any) => {
        return (
            data &&
            data.map((v: any) => [
                {
                    key: "id",
                    isReadOnly: true,
                    type: "field",
                    val: v.id,
                },
                {
                    key: "name",
                    isReadOnly: true,
                    type: "field",
                    val: v.name,
                },
                {
                    key: "level",
                    isReadOnly: true,
                    type: "select",
                    val: v.level,
                    options: levels
                },
                { key: "action", type: ["r", "u", "d"] },
            ])
        );
    };

    const deleteSubject = async (id: any) => {
        if(window.confirm("You want to delete this data?")) {
            const subjects = await fetch(API_URL + `subject/${id}`, {
                method: "DELETE",
            })
            .then(res => res.json())
            .then(res => {
                const newData = state.data.filter((i: any) => i.id !== id);
                setState({ ...state, data: newData });
            })            
        }
    };

    const updateItem = (args: any) => {
        const updatedData = state.data && state.data.map((item: any) => {
            if (item.id === args.id) {
                item = {...item, ...args}
            }
            return item;
        });
        setState({
            ...state,
            data: updatedData, // 'level', 'name', 'date' is a reserved word, so please don't use in db and in row key
        });
    };

    const saveUpdate = (args: any) => {
        fetch(API_URL + `subject/` + args.id, {
            headers: { "Content-Type": "application/json" },
            method: "PUT",
            body: JSON.stringify(args),
        }).then((res) => res.json()).
        then((res) => {
            alert('Data saved!')
        })
    }

    const addItem = (args: any) => {
        console.log({args})
        let url = API_URL + "subject";
        fetch(url, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(args),
        }).then((result) => {
            result.json().then((resp) => {
                alert("Data Added Successfuly");
                const newData = _.cloneDeep(state.data);
                newData.push(args);
                setState({ ...state, data: newData });
            });
        });
    };

    return (
        <>
            <h1>Subjects</h1>
            <DataTable
                header={header}
                hasNew={true}
                rows={invokeRows(state.data || [])}
                actions={{
                    c:addItem,
                    r:"/dashboard/lessons",
                    u:updateItem,
                    d:deleteSubject
                }}
                saveUpdate={saveUpdate}
            />
        </>
    );
};
