import React, { useEffect, useState } from "react";
import { DataTable } from "../../../components/DataTable";
import _ from "lodash";
import { API_URL } from "../../../config";
import { useParams } from "react-router-dom";

export const Students = (props:any) => {
    const {section_id} = useParams()
    const header = [
        "ID #", 
        "First Name",
        "Last Name", 
        "Gender",
        "Section",
        "Grade/Level", 
        "Action"];

    const [state, setState] = useState<any>({});
    const [sections, setSections] = useState<any>([]);
    const [levels, setLevels] = useState<any>([]);

    const getData = async () => {
        const students = await fetch([API_URL + "students", section_id || ''].join('/'), {
            headers: { "Content-Type": "application/json" },
            method: "GET",
        }).then((res) => res.json())
        .then((res) => ( 
            res.map((i:any) => ({
                id : i.user_id,
                fname : i.user.fname,
                mname : i.user.mname,
                lname : i.user.lname,
                gender : i.user.gender,
                section : {
                    value: i.section_id,
                    label: i.section.name
                },
                level : {
                    value: i.level_id,
                    label: i.level.name
                }
            }))
        ))
        students && setState({ data: students });
    };

    // This can be improved in the future by loading at first after login.
    const getSections = async () => {
        const sections = await fetch(API_URL + "sections", {
            headers: { "Content-Type": "application/json" },
            method: "GET",
        }).then((res) => res.json())
        .then((res) => res.map((i:any) => ({
            value: i.id,
            label : i.name
        })));
        sections && setSections(sections)
    }

    // This can be improved in the future by loading at first after login.
    const getGradeLevel = async () => {
        const levels = await fetch(API_URL + "levels", {
            headers: { "Content-Type": "application/json" },
            method: "GET",
        }).then((res) => res.json())
        .then((res) => res.map((i:any) => ({
            value: i.id,
            label : i.name
        })));
        levels && setLevels(levels)
    }

    useEffect(() => {
        getData();
        getSections();
        getGradeLevel();
    }, []);
    
    const invokeRows = (data: any) => {
        return (
            data &&
            data.map((v: any) => [
                {
                    key: "id",
                    isReadOnly: true,
                    type: "field",
                    val: v.id,
                },
                {
                    key: "fname",
                    isReadOnly: true,
                    type: "field",
                    val: v.fname,
                },
                {
                    key: "lname",
                    isReadOnly: true,
                    type: "field",
                    val: v.lname,
                },
                {
                    key: "gender",
                    isReadOnly: true,
                    type: "field",
                    val: v.gender,
                },
                {
                    key: "section",
                    isReadOnly: true,
                    type: "select",
                    val: v.section,
                    options: sections
                },
                {
                    key: "level",
                    isReadOnly: true,
                    type: "select",
                    val: v.level,
                    options: levels
                },
                { key: "action", type: ["r", "u", "d"] },
            ])
        );
    };

  
    const deleteStudent = async (id: any) => {
        if(window.confirm("You want to delete this data?")) {
            const students = await fetch(API_URL + `student/${id}`, {
                method: "DELETE",
            })
            .then(res => res.json())
            .then(res => {
                const newData = state.data.filter((i: any) => i.id !== id);
                setState({ ...state, data: newData });
            })            
        }
    };

    const updateItem = (args: any) => {
        const updatedData = state.data && state.data.map((item: any) => {
            if (item.id === args.id) {
                item = {...item, ...args}
            }
            return item;
        });
        setState({
            ...state,
            data: updatedData, // 'level', 'name', 'date' is a reserved word, so please don't use in db and in row key
        });
    };

    const saveUpdate = (args: any) => {
        fetch(API_URL + `student/` + args.id, {
            headers: { "Content-Type": "application/json" },
            method: "PUT",
            body: JSON.stringify(args),
        }).then((res) => res.json()).
        then((res) => {
            alert('Data saved!')
        })
    }

    const addItem = (args: any) => {
        console.log({args})
        let url = API_URL + "student";
        fetch(url, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(args),
        }).then((result) => {
            result.json().then((resp) => {
                alert("Data Added Successfuly");
                const newData = _.cloneDeep(state.data);
                newData.push(args);
                setState({ ...state, data: newData });
            });
        });
    };

    return (
        <>
            <h1>Students</h1>
            <DataTable
                header={header}
                hasNew={true}
                rows={invokeRows(state.data || [])}
                actions={{
                    c : addItem,
                    r : '/dashboard/profile',
                    d : deleteStudent,
                    u : updateItem,
                }}
                saveUpdate={saveUpdate}
            />
        </>
    );
};
