import React, { useEffect, useState } from "react";
import { DataTable } from "../../../components/DataTable";
import _ from "lodash";
import { API_URL } from "../../../config";
import { useParams } from "react-router-dom";

export const Section = (props:any) => {
    const {level_id} = useParams()
    const header = ["#", "Section Name", "Grade Level", "", "Action"];
    const [state, setState] = useState<any>({});

    const getData = async () => {
        const sections = await fetch([API_URL + "sections", level_id || ''].join('/'), {
            headers: { "Content-Type": "application/json" },
            method: "GET",
        }).then((res) => res.json());
        sections && setState({ data: sections });
    };

    useEffect(() => {
        getData();
    }, []);

    const invokeRows = (data: any) => {
        console.log({data})
        return (
            data &&
            data.map((v: any) => [
                {
                    key: "id",
                    isReadOnly: true,
                    type: "field",
                    val: v.id,
                },
                {
                    key: "name",
                    isReadOnly: true,
                    type: "field",
                    val: v.name,
                },
                {
                    key: "level",
                    isReadOnly: true,
                    type: "field",
                    val: v.level?.name,
                },
                {
                    key: "level_img",
                    isReadOnly: true,
                    type: "thumbnail",
                    val: v.level?.img,
                },
                { key: "action", type: ["r", "u", "d"] },
            ])
        );
    };

    const deleteSection = async (id: any) => {
        if(window.confirm("You want to delete this data?")) {
            const sections = await fetch(API_URL + `section/${id}`, {
                method: "DELETE",
            })
            .then(res => res.json())
            .then(res => {
                const newData = state.data.filter((i: any) => i.id !== id);
                setState({ ...state, data: newData });
            })            
        }
    };

    const updateItem = (args: any) => {
        const updatedData = state.data && state.data.map((item: any) => {
            if (item.id === args.id) {
                item = {...item, ...args}
            }
            return item;
        });
        setState({
            ...state,
            data: updatedData, // 'level', 'name', 'date' is a reserved word, so please don't use in db and in row key
        });
    };

    const saveUpdate = (args: any) => {
        fetch(API_URL + `section/` + args.id, {
            headers: { "Content-Type": "application/json" },
            method: "PUT",
            body: JSON.stringify(args),
        }).then((res) => res.json()).
        then((res) => {
            alert('Data saved!')
        })
    }

    const addItem = (args: any) => {
        console.log({args})
        let url = API_URL + "section";
        fetch(url, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(args),
        }).then((result) => {
            result.json().then((resp) => {
                alert("Data Added Successfuly");
                const newData = _.cloneDeep(state.data);
                newData.push(args);
                setState({ ...state, data: newData });
            });
        });
    };

    return (
        <>
            <h1>Sections</h1>
            <DataTable
                header={header}
                hasNew={true}
                rows={invokeRows(state.data || [])}
                actions={{
                    c:addItem,
                    r:"/dashboard/students",
                    u:updateItem,
                    d:deleteSection
                }}
                saveUpdate={saveUpdate}
            />
        </>
    );
};
