import React, { useState, useCallback, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { FormControl, InputLabel, Select, makeStyles, Button } from '@material-ui/core';
import classes from '*.module.css';
import { API_URL } from "../../../config";
import { result } from 'lodash';
import { useParams } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));

export const Profiles = () => {
    
  const classes = useStyles();
  const tokenkey = localStorage.getItem('userToken');
  const [state, setState] = React.useState({});

    const [allValues, setAllValues] = useState<any>({
      id: 0,
      fname: '',
      mname: '',
      lname: '',
      school: '',
      contactnumber: '',
      gender: '',
      email: '',
      password: '',
      c_password: ''
  });
      
  const changeHandler = (e:any) => {
    setAllValues({...allValues, [e.target.name]: e.target.value})
  }

    const currentUser = (tokenkey: any) => {
      let url = API_URL + "auth/user";
        fetch(url, {
          method: "GET",
          headers: new Headers({
            Authorization: `Bearer `+ tokenkey,
          }),
        }).then((res) => res.json())
        //access current user data
        .then((result) => {
          alert([result.id + "\n" + result.fname + "\n" + result.mname + "\n" + result.lname + "\n" + result.email])
        }
      )
    }  

    const fetchData = useCallback(() => {
      //let url = API_URL + "auth/user";
        fetch(API_URL + "auth/user",  {
          method: "GET",
          headers: new Headers({
            Authorization: `Bearer `+ tokenkey,
          }),
        }).then((res) => res.json())
            .then((res) => {
              setAllValues({
                id: res.id,
                fname: res.fname,
                mname: res.mname,
                lname: res.lname,
                school: res.school,
                contactnumber: res.contactnumber,
                gender: res.gender,
                email: res.email
              })
            })
            .catch((error) => {
                console.log(error)
            })
    }, [])
    
    useEffect(() => {
        fetchData()
    }, [])

    const save = () => {
      fetch(API_URL + "auth/update/" + allValues.id, {
          method: "PUT",
          body : JSON.stringify({
              ...allValues
          }),
          headers: {
              'Content-Type': 'application/json'
          }
      }).then(res => res.json())
      .then((res) => {
          alert('Saved')
      })
  }

    const handleChange = (event:any) => {
      const name = event.target.name;
      setState({
        ...state,
        [name]: event.target.value,
      });
    };

    
    return (
          <React.Fragment>
            <Typography variant="h6" gutterBottom>
              Profile
              <br/>
            </Typography>
      
            <form noValidate>
            <Grid container spacing={3}>
            <Grid item xs={6} sm={4}>
                <TextField
                  value={allValues.fname} onChange={changeHandler}
                  required
                  id="fname"
                  name="fname"
                  label="First Name"
                  fullWidth
                 
                />
              </Grid>
              <Grid item xs={6} sm={4}>
                <TextField
                  value={allValues.mname} onChange={changeHandler}
                  required
                  id="mname"
                  name="mname"
                  label="Middle name"
                  fullWidth
                  autoComplete="family-name"
                />
              </Grid>
              <Grid item xs={6} sm={4}>
                <TextField
                  value={allValues.lname} onChange={changeHandler}
                  required
                  id="lname"
                  name="lname"
                  label="Last name"
                  fullWidth
                  autoComplete="family-name"
                />
              </Grid>
              <Grid item xs={6} sm={6}>
                <TextField
                  value={allValues.school} onChange={changeHandler}
                  required
                  id="school"
                  name="school"
                  label="School"
                  fullWidth
                />
              </Grid>
              <Grid item xs={6} sm={4}>
                <TextField
                  value={allValues.contactnumber} onChange={changeHandler}
                  required
                  id="contactnumber"
                  name="contactnumber"
                  label="Contact Number"
                  fullWidth
                  autoComplete="family-name"
                />
              </Grid>
      
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="gender-native-simple">Gender</InputLabel>
                <Select
                  native
                  onChange={handleChange}>
                  <option value="Female">Female</option>
                  <option value="Male">Male</option>
                </Select>
              </FormControl>
            
              <Grid item xs={12} sm={6}>
                <TextField
                  value={allValues.email} onChange={changeHandler}
                  required
                  id="email"
                  name="email"
                  label="Email"
                  fullWidth
                  autoComplete="email"
                />
              </Grid>
      
              <Grid item xs={12} sm={6}>
                <TextField 
                value={allValues.password} onChange={changeHandler}
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                fullWidth 
                />
              </Grid>
      
              <Button
                    onClick={() => {
                      save()
                  }}
                 
                  fullWidth
                  variant="contained"
                  color="primary"
                  
                  >
                    Update
                  </Button>
              
            </Grid>
            </form>
          </React.Fragment>
        );
      }