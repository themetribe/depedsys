import React, { useState, useEffect, useCallback } from "react"
import { Button, FormControl, TextField } from "@material-ui/core"
import SaveIcon from '@material-ui/icons/Save';
import { title } from "process";
import { invokeTextField, invokeSelectField } from "../../../components/DataTable/utils";
import { Graph } from "react-d3-graph";
import { DataGrid } from "../../../components/DataGrid";
import { APP_DOMAIN, API_URL } from "../../../config";
import "./style.scss";
import ImageBox from "../../../components/ImageBox";
import { useParams } from "react-router-dom";
import _ from "lodash";

const Profile = (props: any) => {
  const {id} = useParams()
  const [state, setState] = useState<any>({
    currentData : {
      fname : '',
      lname : '',
      gender : '',
      bdate : '',
      email : '',
      address : '',
      contact : '',
      level : {},
      section : {},
      avatar : [APP_DOMAIN, 'img/default-avatar.jpg'].join('')
    }
  });
  const [sections, setSections] = useState<any>([]);
  const [levels, setLevels] = useState<any>([]);

  const save = () => {
    fetch(API_URL + "user/"+id, {
        method: "PUT",
        body : JSON.stringify(state.currentData),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .then((res) => {
        alert('Saved')
    })
}

  const fetchData = useCallback(() => {
    fetch(API_URL + "user/" + id).then(res => res.json())
    .then((res) => {
        setState({
          ...state, 
          currentData : {
            ...res,
            section : {
              value: res.section.id,
              label: res.section.name
            },
            level : {
              value: res.level.id,
              label: res.level.name
            }
          }
        })
    })
    .catch((error) => {
        console.log(error)
    })
}, [])

useEffect(() => {
    fetchData()
    getSections()
    getGradeLevel()
}, [])

  // graph payload (with minimalist structure)
  const data = {
    nodes: [{ id: "Harry" }, { id: "Sally" }, { id: "Alice" }],
    links: [
      { source: "Harry", target: "Sally" },
      { source: "Harry", target: "Alice" },
    ],
  };

  // the graph configuration, you only need to pass down properties
  // that you want to override, otherwise default ones will be used
  const myConfig = {
    nodeHighlightBehavior: true,
    node: {
      color: "lightgreen",
      size: 120,
      highlightStrokeColor: "blue",
    },
    link: {
      highlightColor: "lightblue",
    },
  };

  // graph event callbacks
  const onClickGraph = function () {
    window.alert(`Clicked the graph background`);
  };

  const onClickNode = function (nodeId: any) {
    window.alert(`Clicked node ${nodeId}`);
  };

  const onDoubleClickNode = function (nodeId: any) {
    window.alert(`Double clicked node ${nodeId}`);
  };

  const onRightClickNode = function (event: any, nodeId: any) {
    window.alert(`Right clicked node ${nodeId}`);
  };

  const onMouseOverNode = function (nodeId: any) {
    window.alert(`Mouse over node ${nodeId}`);
  };

  const onMouseOutNode = function (nodeId: any) {
    window.alert(`Mouse out node ${nodeId}`);
  };

  const onClickLink = function (source: any, target: any) {
    window.alert(`Clicked link between ${source} and ${target}`);
  };

  const onRightClickLink = function (event: any, source: any, target: any) {
    window.alert(`Right clicked link between ${source} and ${target}`);
  };

  const onMouseOverLink = function (source: any, target: any) {
    window.alert(`Mouse over in link between ${source} and ${target}`);
  };

  const onMouseOutLink = function (source: any, target: any) {
    window.alert(`Mouse out link between ${source} and ${target}`);
  };

  const onNodePositionChange = function (nodeId: any, x: any, y: any) {
    window.alert(`Node ${nodeId} is moved to new position. New position is x= ${x} y= ${y}`);
  };

  // This can be improved in the future by loading at first after login. This is also cloned from students. please refactor in the future.
  const getSections = async () => {
    const sections = await fetch(API_URL + "sections", {
        headers: { "Content-Type": "application/json" },
        method: "GET",
    }).then((res) => res.json())
    .then((res) => res.map((i:any) => ({
        value: i.id,
        label : i.name
    })));
    sections && setSections(sections)
}

// This can be improved in the future by loading at first after login. This is also cloned from students. please refactor in the future.
const getGradeLevel = async () => {
    const levels = await fetch(API_URL + "levels", {
        headers: { "Content-Type": "application/json" },
        method: "GET",
    }).then((res) => res.json())
    .then((res) => res.map((i:any) => ({
        value: i.id,
        label : i.name
    })));
    levels && setLevels(levels)
}

  const mapForTextField = (val:any) => ({
    key : [val],
    val: state.currentData[val] // this will get the state set dynamically, e.g. fname
  })
  const mapForSelectField = (val:any, options:any) => ({
    key : [val],
    val: state.currentData[val], // this will get the state set dynamically, e.g. fname
    options: options
  })
  return (
    <>
      <div className="flex-apart">
        <h2>Profile</h2>
        <Button
          onClick={save}
          variant="contained"
          color="primary"
          size="large"
          endIcon={<SaveIcon />}
        >
          Save
                </Button>
      </div>

      <ImageBox imgSrc={state.currentData.avatar} />

      <h4>Basic Information</h4>
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeTextField(state, setState, mapForTextField('fname'), 'First Name')}
        </FormControl>
      </div>
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeTextField(state, setState, mapForTextField('lname'), 'Last Name')}
        </FormControl>
      </div>
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeTextField(state, setState, mapForTextField('gender'), 'Gender')}
        </FormControl>
      </div>
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeTextField(state, setState, mapForTextField('bdate'), 'Birth Date')}
        </FormControl>
      </div>
      <hr />
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeTextField(state, setState, mapForTextField('email'), 'Email')}
        </FormControl>
      </div>
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeTextField(state, setState, mapForTextField('address'), 'Address')}
        </FormControl>
      </div>
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeTextField(state, setState, mapForTextField('contact'), 'Contact')}
        </FormControl>
      </div>
      <hr />
      <h4>Change Password</h4>
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeTextField(state, setState, { key: 'oldPassword' }, 'Enter Current Password')}
        </FormControl>
      </div>
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeTextField(state, setState, { key: 'newPassword' }, 'Enter New Password')}
        </FormControl>
      </div>
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeTextField(state, setState, { key: 'newPassword2' }, 'Confirm New Password')}
        </FormControl>
      </div>
      <hr />
      <h4>Student Details</h4>
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeSelectField(mapForSelectField('level', levels), 'Grade/Level', state, setState)}
        </FormControl>
      </div>
      <div className="form-fields">
        <FormControl component="fieldset">
          {invokeSelectField(mapForSelectField('section', sections), 'Section', state, setState)}
        </FormControl>
      </div>
      <hr />
      <h4>Progress Graph</h4>
      <div className="progress-graph-container">
        <Graph
          id="graph-id" // id is mandatory, if no id is defined rd3g will throw an error
          data={data}
          config={myConfig}
          onClickNode={onClickNode}
          onDoubleClickNode={onDoubleClickNode}
          onRightClickNode={onRightClickNode}
          onClickGraph={onClickGraph}
          onClickLink={onClickLink}
          onRightClickLink={onRightClickLink}
          onMouseOverNode={onMouseOverNode}
          onMouseOutNode={onMouseOutNode}
          onMouseOverLink={onMouseOverLink}
          onMouseOutLink={onMouseOutLink}
          onNodePositionChange={onNodePositionChange}
        />
      </div>
      <h4>Activity Log</h4>
      <p>under construction...</p>

    </>
  )
}
export default Profile