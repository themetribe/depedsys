import React, { useEffect, useState } from "react";
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { API_URL } from "../../config";
import { isError } from "lodash";
import { Container } from "@material-ui/core";



const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export const Register = () => {
  const classes = useStyles();

  // const [fname, setFname] = useState('');
  // const [mname, setMname] = useState('');
  // const [lname, setLname] = useState('');
  // const [school, setSchool] = useState('');
  // const [contactnumber, setContact] = useState('');
  // const [gender, setGender] = useState('');
  // const [email, setEmail] = useState('');
  // const [password, setPassword] = useState('');
  // const [c_password, setcPassword] = useState('');
 
  const [allValues, setAllValues] = useState({
    fname: '',
    mname: '',
    lname: '',
    school: '',
    contactnumber: '',
    gender: '',
    email: '',
    password: '',
    c_password: ''
 });
 
 const changeHandler = (e:any) => {
  setAllValues({...allValues, [e.target.name]: e.target.value})
}

  const registerFunc = () => {
    let url = API_URL + "auth/register";
      fetch(url, {
        method: "POST",
        headers: {
          'Accept': 'application/json', 
          'Content-Type': 'application/json' 
        },
          body: JSON.stringify({...allValues})
      }).then((res) => res.json())
      .then((result) => {
        //console.log(result)
        if(result.message === 'Success!'){
          window.location.href= '/'
          alert('You are now registed. Please Login')
        }
        else if(result.message === 'The given data was invalid.'){
         // window.location.href= '/register'
          alert(result.errors.email)
        }
        
        else {
          alert(result.error)
        }
       }
     )
   }


  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
          
          <Grid item xs={12}>
              <TextField
                value={allValues.fname} onChange={changeHandler}
                required
                fullWidth
                id="firstName"
                label="First Name"
                name="fname"
              />
            </Grid>
           
            <Grid item xs={12}>
              <TextField
                value={allValues.mname} onChange={changeHandler}
                required
                fullWidth
                id="middleName"
                label="Middle Name"
                name="mname"
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                value={allValues.lname} onChange={changeHandler}
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lname"
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                value={allValues.school} onChange={changeHandler}
                required
                fullWidth
                id="school"
                label="School"
                name="school"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                 value={allValues.contactnumber} onChange={changeHandler}
                required
                fullWidth
                id="contactnumber"
                label="Contact Number"
                name="contactnumber"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                value={allValues.gender} onChange={changeHandler}
                required
                fullWidth
                id="gender"
                label="gender"
                name="gender"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                value={allValues.email} onChange={changeHandler}
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                value={allValues.password} onChange={changeHandler}
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                //autoComplete="current-password"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                value={allValues.c_password} onChange={changeHandler}
                required
                fullWidth
                name="c_password"
                label="Password"
                type="password"
                id="c_password"
                //autoComplete="current-password"
              />
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="I want to receive updates via email."
              />
            </Grid>
          </Grid>
          <Button onClick={() => {
                 registerFunc();
            }}
          
            fullWidth
            variant="contained"
            color="primary"
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
      
      </Box>
    </Container>
  );
}