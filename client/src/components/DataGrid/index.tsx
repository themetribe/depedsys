import React, { useState, useEffect } from "react";
import "./style.scss";
import Modal from "../../global/Modal";
import { Button, FormControl, TextField, IconButton, Select } from "@material-ui/core";
import { Redirect, Link } from "react-router-dom";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import SendIcon from '@material-ui/icons/Send';
import VisibilityIcon from "@material-ui/icons/Visibility";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { AnyCnameRecord } from "dns";


interface DataGrid_i {
    datas : any[],
    hasNew : Boolean,
    defaultData: any,
    actions?: any; // same with DataTable actions
    saveUpdate?: any;    
}
export const DataGrid = (props : DataGrid_i) => {
    const { datas, hasNew } = props;
    const FIRST_ITEM_INDEX = 0;

    const [header, setHeader] = useState<any>([]); // separate because datagrid no need header, so we should fetch from one of the data
    
    const [state, setState] = useState<any>({
        showAddModal: false,
        showEditModal: false,
        selectedRowId: FIRST_ITEM_INDEX,
        currentData: {},
        newFields: [] // the fields to generate the new form modal
    });

    useEffect(() => {
        setHeader(props.defaultData.map((d:any) => d.key))
    }, [props.datas])
    useEffect(() => {
        props.actions?.u && props.actions.u(state.currentData, state.selectedRowId)
    }, [state.currentData])

    useEffect(() => {
        console.log({newFields : state.newFields})
    }, [state.newFields])

    // Methods
    const handleAdd = () => {
        const data = Array.isArray(props.datas[FIRST_ITEM_INDEX]) 
        ? props.datas[FIRST_ITEM_INDEX]
        : props.defaultData
        const newFields = [...data.map((i: any) => ({ 
            ...i,
            val: ''
        }))]

        
        
        setState({ ...state, showAddModal: true, selectedRowId: null, newFields });
    };
    const closeModal = () => {
        setState({ ...state, showAddModal: false, showEditModal: false, newFields: [] });
    };
    const handleEdit = (selectedRowId: Number) => {
        setState({ ...state, showEditModal: true, selectedRowId })
    }

    // Render Methods
    const renderFormField = (item: any, label: String) => {
        console.log({ item })
        switch (item.type) {
            case "field":
                return (
                    <div className="form-fields">
                        <FormControl component="fieldset">
                            <TextField
                                onChange={(e: any) => {
                                    // props.updateFunc()
                                    // update if in new modal state (checks if newFIelds exists (meaning if new modal))
                                    const newFields = state.newFields ? state.newFields.map((i: any) => {
                                        if (i.key === item.key) {
                                            i.val = e.target.value
                                        }
                                        return i;
                                    }) : [];
                                    setState({
                                        ...state,
                                        currentData: {
                                            ...state.currentData,
                                            [item.key]: e.target.value,
                                            id: state.selectedRowId
                                        },
                                        newFields
                                    })
                                }}
                                id="outlined-basic"
                                label={label}
                                variant="outlined"
                                value={item.val}

                            />
                        </FormControl>
                    </div>
                );
            // add the other types here....
        }
    };
    const addAndEditMainModal = (args: any) => {
        return (
            <Modal
                onClose={() => {
                    closeModal();
                }}
                showModal={args.showHideState}
            >
                <h4>{args.title}</h4>
                <form noValidate autoComplete="off">
                    {args.renderFieldFunc()}
                    <Button
                        variant="contained"
                        color="primary"
                        size="large"
                        endIcon={<SendIcon />}
                        onClick={() => {
                            args.buttonAction()
                            closeModal()
                        }}
                    >
                        {args.buttonLabel}
                    </Button>
                </form>
            </Modal>
        )
    }
    const renderAddModal = () => {
        return (
            addAndEditMainModal({
                title: "Add Details",
                renderFieldFunc: () => {
                    return (
                        state.newFields && state.newFields.map((item: any, i: any) =>
                            (renderFormField({ ...item }, header[i])))
                    )
                },
                showHideState: state.showAddModal,
                buttonAction: () => { props.actions?.c(state.currentData) },
                buttonLabel: "Add"
            })
        )
    }
    const renderEditModal = () => {
        return (
            addAndEditMainModal({
                title: "Edit Details",
                renderFieldFunc: () => {
                    return (
                        datas?.find((item: any, index: any) =>
                            item.find((i: any) => i.key === "id" && i.val === state.selectedRowId)
                        )?.map((item: any, i: any) =>
                            (renderFormField(item, header[i])))
                    )
                },
                showHideState: state.showEditModal,
                buttonAction: () => { props.saveUpdate(state.currentData) },
                buttonLabel: "Save",

            }))
    }
    const renderCell = (item: any, index: any) => {

        switch (item.key) {
            case "name":
                return (<span>{item.val}</span>);
            case "img":
                return (<img className="cell-img" src={item.val.match(/http/gi) ? item.val : ["/img",item.val].join('/')} />);
            default: {
                if (Array.isArray(item.type)) {
                    return (
                        <div className="actions">
                            {item.type.find((i: String) => i === "r") && (
                                <Link to={[props.actions?.r, index].join('/')}>
                                    <IconButton>
                                        <VisibilityIcon></VisibilityIcon>
                                    </IconButton>
                                </Link>
                            )}
                            {item.type.find((i: String) => i === "u") && (
                                <IconButton onClick={() => {
                                    handleEdit(index)
                                }}>
                                    <EditIcon></EditIcon>
                                </IconButton>
                            )}
                            {item.type.find((i: String) => i === "d") && (
                                <IconButton onClick={() => {
                                    props.actions?.d(index)
                                }}>
                                    <DeleteIcon></DeleteIcon>
                                </IconButton>
                            )}
                        </div>
                    );
                }
            }
        }

    }

    return (
        <>
            {/* ======[ Modal Section ]======= */}
            {renderAddModal()}
            {renderEditModal()}
            
            <div className="datagrid-container">
                
                {datas.map((items:any, index:any) => (
                    
                    <div className="cell-container">
                        <div className="cell">
                            {items.map((item: any) => (renderCell(item, items.find((i: any) => i.key === 'id').val)))}
                        </div>
                    </div>                    
                ))}
                {hasNew && (
                    <div className="cell-container">
                        <div className="cell">
                            <IconButton
                                size="medium"
                                edge="start"
                                classes={{ root: "btnNew" }}
                                onClick={handleAdd}
                            >
                                <AddCircleIcon fontSize="large" />
                            </IconButton>
                            <span>Add New</span>
                        </div>
                    </div>
                    
                )}
            </div>
        </>
    );
};
