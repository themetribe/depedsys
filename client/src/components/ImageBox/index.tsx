import { APP_DOMAIN } from "../../config"
import EditIcon from "@material-ui/icons/Edit";
import React from "react"
import { IconButton } from "@material-ui/core";
import "./style.scss"
import { getRoles } from "@testing-library/react";

const ImageBox = (props:any) => {
    const handleEdit = () => {
        alert('under construction')
    }
    return (
        <div className="image-box-container">
            <div className="actions">
                <IconButton onClick={() => {handleEdit()}}><EditIcon></EditIcon></IconButton>
            </div>
            <img className="avatar-container" src={props.imgSrc} />
        </div>
    )
}

export default ImageBox