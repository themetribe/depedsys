import React, { useState, useEffect } from "react";
import "./style.scss";
import {
    TableContainer,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Table,
    Paper,
    makeStyles,
    ButtonGroup,
    Button,
    Select,
    OutlinedInput,
    InputAdornment,
    ThemeProvider,
    TextField,
    IconButton,
    FormControl,
} from "@material-ui/core";
import moment from 'moment';
import VisibilityIcon from "@material-ui/icons/Visibility";
import EditIcon from "@material-ui/icons/Edit";
import SendIcon from '@material-ui/icons/Send';
import DeleteIcon from "@material-ui/icons/Delete";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import _ from "lodash";
import ReactDOM from "react-dom";
import Modal from "../../global/Modal";
import { Redirect, Link } from "react-router-dom";
import { invokeTextField, invokeSelectField } from "./utils";
interface DataTable_i {
    rows: any[]; //Data_i[]
    header: String[];
    hasNew?: Boolean;
    saveUpdate?: any;
    actions?: any; // this will replace deleteFunc, updateFunc, addFunc, viewHref, will accept c,r,u,d instead. Note: v = a template component e.g <Lesson />
}
interface DataTableState_i {
    showAddModal: boolean;
    showEditModal: boolean;
    selectedRowId: Number | null;
    currentData: any;
    newFields: any[];
}
const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

const formatMoney = (
    amount: any,
    decimalCount = 2,
    decimal = ".",
    thousands = ","
) => {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        const i = parseInt(
            (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))
        );
        const j = i.toString().length > 3 ? i.toString().length % 3 : 0;

        return (
            negativeSign +
            (j ? i.toString().substr(0, j) + thousands : "") +
            i
                .toString()
                .substr(j)
                .replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
            (decimalCount
                ? decimal +
                Math.abs(amount - i)
                    .toFixed(decimalCount)
                    .slice(2)
                : "")
        );
    } catch (e) {
        console.log(e);
    }
};

export const DataTable = (props: DataTable_i) => {
    const { rows, header, hasNew } = props;
    const FIRST_ITEM_INDEX = 0;

    const [state, setState] = useState<DataTableState_i>({
        showAddModal: false,
        showEditModal: false,
        selectedRowId: FIRST_ITEM_INDEX,
        currentData: {},
        newFields: [] // the field to generate the new form modal
    });

    useEffect(() => {
        props.actions?.u(state.currentData, state.selectedRowId)
    }, [state.currentData])

    const classes = useStyles();

    // Methods
    const handleAdd = () => {
        const newFields = [...props.rows[FIRST_ITEM_INDEX].map((i: any) => ({
            ...i,
            val: ''
        }))]
        setState({ ...state, showAddModal: true, selectedRowId: null, newFields });
    };
    const closeModal = () => {
        setState({ ...state, showAddModal: false, showEditModal: false, newFields: [] });
    };
    const handleEdit = (selectedRowId: Number) => {
        setState({ ...state, showEditModal: true, selectedRowId })
    }

    // Render Methods
    const addAndEditMainModal = (args: any) => {
        return (
            <Modal
                onClose={() => {
                    closeModal();
                }}
                showModal={args.showHideState}
            >
                <h4>{args.title}</h4>
                <form noValidate autoComplete="off">
                    {args.renderFieldFunc()}
                    <Button
                        variant="contained"
                        color="primary"
                        size="large"
                        endIcon={<SendIcon />}
                        onClick={() => {
                            args.buttonAction()
                            closeModal()
                        }}
                    >
                        {args.buttonLabel}
                    </Button>
                </form>
            </Modal>
        )
    }
    const renderFormField = (item: any, label: String) => {
        let ret;
        switch (item.type) {
            case "field": ret = invokeTextField(state,setState,item,label); break;
            case "select": ret = invokeSelectField(item,label, state, setState); break;
        }
        return (
            <div className="form-fields">
                <FormControl component="fieldset">
                    {(ret)}
                </FormControl>
            </div>
        );
    };

    const renderAddModal = () => {
        return (
            addAndEditMainModal({
                title: "Add Details",
                renderFieldFunc: () => {
                    return (
                        state.newFields && state.newFields.map((item: any, i: any) =>
                            (renderFormField(item, header[i])))
                    )
                },
                showHideState: state.showAddModal,
                buttonAction: () => { props.actions?.c(state.currentData) },
                buttonLabel: "Add"
            })
        )
    }
    const renderEditModal = () => {
        return (
            addAndEditMainModal({
                title: "Edit Details",
                renderFieldFunc: () => {
                    return (
                        rows?.find((item: any, index: any) =>
                            item.find((i: any) => i.key === "id" && i.val === state.selectedRowId)
                        )?.map((item: any, i: any) =>
                            (renderFormField(item, header[i])))
                    )
                },
                showHideState: state.showEditModal,
                buttonAction: () => { props.saveUpdate(state.currentData) },
                buttonLabel: "Save",

            }))
    }


    const renderCell = (item: any, index: any) => {
        switch (item.type) {
            case "thumbnail": 
                return <img src={item.val} className="thumbnail" alt={item.name} title={item.name} />
            case "spacer":
                return <span>&nbsp;</span>;
            case "select":
                if (item.isReadOnly) 
                    return <span>{item.val.label}</span>
                else 
                    return invokeSelectField(item)
            case "currency":
                if (item.isReadOnly) return <span>${item.val}</span>;
                else
                    return (
                        <OutlinedInput
                            inputProps={{ maxlength: 7 }}
                            margin="none"
                            value={item.val}
                            startAdornment={
                                <InputAdornment position="start">$</InputAdornment>
                            }
                            onChange={
                                (e: React.ChangeEvent<{ value: any }>) =>
                                    alert("Under construction")
                                // modifyRow(item, index, e.target.value)
                            }
                        />
                    );
            case "date":
                const curDate = moment.utc(item.val).format('DD/MM/YYYY');
                return (
                    <span>{curDate}</span>
                    //   <ThemeProvider>
                    //     <MuiPickersUtilsProvider
                    //       libInstance={moment}
                    //       utils={MomentUtils}
                    //       locale="au"
                    //     >
                    //       <KeyboardDatePicker
                    //         autoOk
                    //         variant="inline"
                    //         format="DD-MM-YYYY"
                    //         InputProps={{ className: styles.dateInput }}
                    //         value={moment(new Date(item.val)).format("YYYY-MM-DD")}
                    //         onChange={date => modifyRow(item, index, date?.format())}
                    //       />
                    //     </MuiPickersUtilsProvider>
                    //   </ThemeProvider>
                );
            case "field":
                if (item.isReadOnly) {
                    if (item.key === "address" && Array.isArray(item.val)) {
                        return <span>{item.val.join(", ")}</span>;
                    } else {
                        return <span>{item.val}</span>;
                    }
                } else
                    return (
                        <TextField
                            size="small"
                            variant="outlined"
                            value={item.val}
                            onChange={
                                (e: React.ChangeEvent<{ value: any }>) =>
                                    alert("Under construction")
                                // modifyRow(item, index, e.target.value)
                            }
                        />
                    );
            case "showMoreDetails":
                return (
                    <span
                        aria-label="expand row"
                        onClick={() => {
                            alert("Under construction");
                            //   return modifyRow(item, index, !item.val)
                        }}
                    >
                        {item.val ? (
                            <KeyboardArrowUpIcon fontSize="large" />
                        ) : (
                                <KeyboardArrowDownIcon fontSize="large" />
                            )}
                    </span>
                );
            case "percentage":
                if (item.isReadOnly) return <span>{item.val}%</span>;
                else
                    return (
                        <OutlinedInput
                            margin="dense"
                            value={item.val}
                            endAdornment={
                                <InputAdornment position="end">%</InputAdornment>
                            }
                            onChange={
                                (e: React.ChangeEvent<{ value: any }>) =>
                                    alert("Under construction")
                                // modifyRow(item, index, e.target.value)
                            }
                        />
                    );
            case "switch":
                return (
                    <span>Under construction</span>
                    //   <OrangeSwitch
                    //     size="small"
                    //     checked={item.val}
                    //     onChange={() => {
                    //       if (item.key === "isRefi") {
                    //         console.log("ROWS", rows)
                    //         _.mapValues(rows[0], function(row) {
                    //           if (row.key === "repayment" || row.key === "balance") {
                    //             row.isComputed = !item.val
                    //             row.isForTotal = !item.val
                    //           }
                    //         })
                    //         console.log("UPDATED", rows)
                    //       }
                    //       alert('Under construction')
                    //     //   modifyRow(item, index, !item.val)
                    //     }}
                    //   />
                );
            case "moredetails":
                return (
                    <>
                        <Table size="small" aria-label="purchases">
                            <TableBody>
                                {item.details &&
                                    item.details.map((detail: any) => (
                                        <TableRow key={detail.flexType}>
                                            <TableCell>{detail.desc}</TableCell>
                                            <TableCell>
                                                <span>
                                                    $
                                                    {!_.isNil(detail.monthlyAmount)
                                                        ? formatMoney(
                                                            detail.monthlyAmount
                                                        )
                                                        : 0}
                                                </span>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                            </TableBody>
                        </Table>
                    </>
                );
            default: {
                // for key=action
                if (Array.isArray(item.type)) {
                    return (
                        <>
                            {item.type.find((i: String) => i === "r") && (
                                <Link to={[props.actions?.r, index].join('/')}>
                                    <IconButton>
                                        <VisibilityIcon></VisibilityIcon>
                                    </IconButton>
                                </Link>
                            )}
                            {item.type.find((i: String) => i === "u") && (
                                <IconButton onClick={() => {
                                    handleEdit(index)
                                }}>
                                    <EditIcon></EditIcon>
                                </IconButton>
                            )}
                            {item.type.find((i: String) => i === "d") && (
                                <IconButton onClick={() => {
                                    props.actions?.d(index)
                                }}>
                                    <DeleteIcon></DeleteIcon>
                                </IconButton>
                            )}
                        </>
                    );
                }
            }
        }
    };

    // console.log({rows})
    return (
        <>
            {/* ======[ Modal Section ]======= */}
            {renderAddModal()}
            {renderEditModal()}
            {/* ============================== */}

            <TableContainer component={Paper} className="datatable-container">
                {hasNew && (
                    <IconButton
                        size="medium"
                        edge="start"
                        classes={{ root: "btnNew" }}
                        onClick={handleAdd}
                    >
                        <AddCircleIcon fontSize="large" />
                    </IconButton>
                )}
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            {header.map((th) => (
                                <TableCell>{th}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows && rows.map((items, i) => (
                            <TableRow>
                                {items.map((item: any, index: any) => (
                                    <TableCell key={index}>
                                        {renderCell(item, items.find((i: any) => i.key === 'id').val)}
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    );
};
