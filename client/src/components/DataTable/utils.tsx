import { Select, TextField } from "@material-ui/core";
import React from "react";

export const invokeTextField = (state:any, setState:Function, item:any, label:String) => {
    
    return (
        <TextField
            onChange={(e: any) => {
                // props.updateFunc()
                // update if in new modal state (checks if newFIelds exists (meaning if new modal))
                const newFields = state.newFields ? state.newFields.map((i: any) => {
                    if (i.key === item.key) {
                        i.val = e.target.value
                    }
                    return i;
                }) : [];
                setState({
                    ...state,
                    currentData: {
                        ...state.currentData,
                        [item.key]: e.target.value,
                        id: state.selectedRowId
                    },
                    newFields
                })
            }}
            id="outlined-basic"
            label={label}
            variant="outlined"
            value={item.val}

        />
    )
}

export const invokeSelectField = (item:any, label?:String, state?:any, setState?:Function) => {
    return (
        <Select
            native
            variant="outlined"
            label={label || ''}
            value={item.val.value}
            onChange={
                (e: React.ChangeEvent<{ value: any }>) => {
                    const newFields = state?.newFields ? state.newFields.map((i: any) => {
                        if (i.key === item.key) {
                            i.val = {
                                value: e.target.value,
                                label: item.options.find((i:any) => {return i.value === parseInt(e.target.value)}).label
                            }
                        }
                        return i;
                    }) : [];
                    setState && setState({
                        ...state,
                        currentData: {
                            ...state.currentData,
                            [item.key]: {
                                value: e.target.value,
                                label: item.options.find((i:any) => {return i.value === parseInt(e.target.value)}).label
                            },
                            id: state.selectedRowId
                        },
                        newFields
                    })
                }
            }
            style={{ borderColor: item.borderColor || "transparent" }}
        >
            {item.options.map((option: any, index: any) => (
                <option key={index} value={option.value}>
                    {option.label}
                </option>
            ))}
        </Select>
        
        // <TextField
        //     onChange={(e: any) => {
        //         // props.updateFunc()
        //         // update if in new modal state (checks if newFIelds exists (meaning if new modal))
        //         const newFields = state.newFields ? state.newFields.map((i: any) => {
        //             if (i.key === item.key) {
        //                 i.val = e.target.value
        //             }
        //             return i;
        //         }) : [];
        //         setState({
        //             ...state,
        //             currentData: {
        //                 ...state.currentData,
        //                 [item.key]: e.target.value,
        //                 id: state.selectedRowId
        //             },
        //             newFields
        //         })
        //     }}
        //     id="outlined-basic"
        //     label={label}
        //     variant="outlined"
        //     value={item.val}

        // />
    )
}