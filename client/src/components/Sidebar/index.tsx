import React from "react";
import { Link } from "react-router-dom";
import { Avatar } from "@material-ui/core";
import DashboardIcon from '@material-ui/icons/Dashboard';
import GradeIcon from '@material-ui/icons/Grade';
import EventSeatIcon from '@material-ui/icons/EventSeat';
import ChildCareIcon from '@material-ui/icons/ChildCare';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import AssessmentIcon from '@material-ui/icons/Assessment';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SettingsIcon from '@material-ui/icons/Settings';
import ForumIcon from '@material-ui/icons/Forum';

import "./style.scss";
import { parse } from "path";


export const Sidebar = (props:any) => {
    const FIRST_CHARACTER_INDEX = 0;
    const user:any = JSON.parse(localStorage.getItem('_user') || '');
    
    return (
        <>
            <div className="sidebar-container">
                <span className="sidebar-bg"></span>
                <div className="avatar-container">
                    <Avatar className="avatar">{[user?.fname[FIRST_CHARACTER_INDEX], user?.lname[FIRST_CHARACTER_INDEX]].join('') }</Avatar>
                    <strong className="user-name">{[user?.fname,user?.lname].join(' ')}</strong>
                </div>
                <ul>
                    <li>
                        <Link to="/dashboard"><DashboardIcon fontSize="large"></DashboardIcon> Dashboard</Link>
                    </li>
                    <li>
                        <Link to="/dashboard/grade"><GradeIcon fontSize="large"></GradeIcon> Grade Level</Link>
                    </li>
                    <li>
                        <Link to="/dashboard/sections"><EventSeatIcon fontSize="large"></EventSeatIcon>  Section</Link>
                    </li>
                    <li>
                        <Link to="/dashboard/students"><ChildCareIcon fontSize="large"></ChildCareIcon> Students</Link>
                    </li>
                    <li>
                        <Link to="/dashboard/subjects"><ImportContactsIcon fontSize="large"></ImportContactsIcon> Subjects</Link>
                    </li>
                    <li>
                        <Link to="/dashboard/lessons"><ForumIcon fontSize="large"></ForumIcon> Lessons</Link>
                    </li>
                    <li>
                        <Link to="/dashboard/reports"><AssessmentIcon fontSize="large"></AssessmentIcon> Reports</Link>
                    </li>
                </ul>

                <ul>
                    <li>
                        <Link to="/dashboard/settings"><SettingsIcon fontSize="large"></SettingsIcon> Settings</Link>
                    </li>   
                    <li>
                        <Link to="/dashboard/profile"><SettingsIcon fontSize="large"></SettingsIcon> Profile</Link>
                    </li> 
                    <li>
                        <Link to="/" onClick={() => {
                            localStorage.removeItem('_token')
                            props.history?.push('/')
                        }}><ExitToAppIcon fontSize="large"></ExitToAppIcon> Logout</Link>
                    </li>
                </ul>
            </div>
        </>
    );
};
