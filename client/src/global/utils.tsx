import { Route, Redirect } from "react-router-dom";
import React from "react";
import { API_URL } from "../config";

const getSession = () => {
    const jwt = localStorage.getItem('_token')
    let session
    try {
        if (jwt) {
            const base64Url = jwt.split('.')[1]
            const base64 = base64Url.replace('-', '+').replace('_', '/')
            // what is window.atob ?
            // https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/atob
            session = JSON.parse(window.atob(base64))
        }
    } catch (error) {
        console.log(error)
    }
    return session
}

export const ProtectedRoute = ({ component: Component, ...rest }: any) => {
    return (
    <Route
        {...rest}
        render={(props: any) => {
            if (getSession()) {
                return (
                    <Component {...props} />
                )
            }
            else {
                return (<Redirect to={{
                    pathname: '/',
                    state: {
                        from: props.location
                    }
                }} />)
            }
        }}
    />
)}
