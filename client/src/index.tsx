import React from "react";
import ReactDOM from "react-dom";
import { Login } from "./scenes/Login";
import { Dashboard } from "./scenes/Dashboard";
import { Register } from "./scenes/Register";
// import * as serviceWorker from "./serviceWorker";
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import { ProtectedRoute } from "./global/utils";

ReactDOM.render(
    <Router>
      <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/register" component={Register} />
          <ProtectedRoute path="/dashboard" component={Dashboard} />
      </Switch>
    </Router>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
