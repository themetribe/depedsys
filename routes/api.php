<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['api'])->group(function ($router) {
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/logout', 'AuthController@logout');
    Route::post('auth/refresh', 'AuthController@refresh');
    Route::get('auth/me', 'AuthController@me');
    Route::post('auth/register', 'AuthController@register');
    Route::put('auth/update/{id}', 'AuthController@update');
});


//Mentor endpoints http://127.0.0.1:8000/api/<your route depending on your function>
//Route::post('login', 'UserController@login');
/*Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');
Route::post('/logout', 'AuthController@logout')->name('logout');
Route::get('registered', 'UserController@registered'); */

Route::get('/user/{id}', 'UserController@showbyid');
Route::put('/user/{id}', 'UserController@update');

//level endpoints http://127.0.0.1:8000/api/<your route depending on your function>
Route::post('/level', 'LevelController@create'); 
Route::get('/levels', 'LevelController@show');
Route::get('/level/{id}', 'LevelController@showbyid');
Route::put('/level/{id}', 'LevelController@update');
Route::delete('/level/{id}', 'LevelController@delete');


//section endpoints http://127.0.0.1:8000/api/<your route depending on your function>
Route::post('/section', 'SectionController@create');
Route::get('/sections/{id}', 'SectionController@showbylevelid');
Route::get('/sections', 'SectionController@show');
Route::get('/section/{id}', 'SectionController@showbyid');
Route::put('/section/{id}', 'SectionController@update');
Route::delete('/section/{id}', 'SectionController@delete');


//subject endpoints http://127.0.0.1:8000/api/<your route depending on your function>
Route::post('/subject', 'SubjectController@create');
Route::get('/subjects', 'SubjectController@show');
Route::get('/subject/{id}', 'SubjectController@showbyid');
Route::put('/subject/{id}', 'SubjectController@update');
Route::delete('/subject/{id}', 'SubjectController@delete');

//student endpoints http://127.0.0.1:8000/api/<your route depending on your function>
Route::post('/student', 'StudentController@create');
Route::get('/students/{id}', 'StudentController@showbysectionid');
Route::get('/students', 'StudentController@show');
Route::get('/student/{id}', 'StudentController@showbyid');
Route::put('/student/{id}', 'StudentController@update');
Route::delete('/student/{id}', 'StudentController@delete');

//assignment endpoints http://127.0.0.1:8000/api/<your route depending on your function>
Route::post('/assignment', 'AssignmentController@create');
Route::get('/assignments', 'AssignmentController@show');
Route::get('/assignment/{id}', 'AssignmentController@showbyid');
Route::put('/assignment/{id}', 'AssignmentController@update');
Route::delete('/assignment/{id}', 'AssignmentController@delete');

//quiz endpoints http://127.0.0.1:8000/api/<your route depending on your function>
Route::post('/quiz', 'QuizController@create');
Route::get('/quizzes', 'QuizController@show');

# Commented out for now because it might be usefull in the future
#Route::get('/quiz/{id}', 'QuizController@showbyid');
#Route::put('/quiz/{id}', 'QuizController@update');

Route::get('/quiz/{lesson_id}', 'QuizController@showbylessonid');
Route::put('/quiz/{lesson_id}', 'QuizController@updatebylessonid');

Route::delete('/quiz/{id}', 'QuizController@delete');

//exams endpoints http://127.0.0.1:8000/api/<your route depending on your function>
Route::post('/exam', 'ExamController@create');
Route::get('/exams', 'ExamController@show');
Route::get('/exam/{id}', 'ExamController@showbyid');
Route::put('/exam/{id}', 'ExamController@update');
Route::delete('/exam/{id}', 'ExamController@delete');

//lessons endpoints http://127.0.0.1:8000/api/<your route depending on your function>
Route::post('/lesson', 'LessonController@create');
Route::get('/lessons', 'LessonController@show');
Route::get('/lessons/{id}', 'LessonController@showbysubjectid');
Route::get('/lesson/{id}', 'LessonController@showbyid');
Route::put('/lesson/{id}', 'LessonController@update');
Route::delete('/lesson/{id}', 'LessonController@delete');