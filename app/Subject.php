<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];

    public function assignments(){
        return $this->belongsTo('App\Assignment');
    }

    public function quizzes(){
        return $this->belongsTo('App\Quiz');
    }

    public function exams(){
        return $this->belongsTo('App\Exam');
    }

    public function level() {
        return $this->belongsTo('App\Level', 'level_id', 'id');
    }


}
