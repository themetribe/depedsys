<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    protected $guarded = ['id'];
    
    public function user() {
        return $this->belongsTo('App\User');
    }
    public function section(){
        return $this->belongsTo('App\Section');
    }

    public function level(){
        return $this->belongsTo('App\Level');
    }
}
