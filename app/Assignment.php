<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $table = 'assignments';
    protected $guarded = ['id'];

    public function lesson(){
        return $this->belongsTo('App\Lesson','lesson_id');
    }
}
