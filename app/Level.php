<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{

    protected $table = 'levels';
    protected $guarded = ['id'];

    public function levels() {
        return $this->belongsTo('App\Student', 'level_id', 'id');
    }

    public function users(){
        return $this->belongsTo('App\User');
    }
    
}
