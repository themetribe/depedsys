<?php

namespace App\Http\Controllers;

use App\Assignment;
use Illuminate\Http\Request;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $assignments = new Assignment();
        $assignments->name = $request->input('name');
        $assignments->item = $request->input('item');
        $assignments->duedate = $request->input('duedate');
        $assignments->save();
        return response()->json($assignments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $assignments = Assignment::all();
        return response()->json($assignments);
    }

    public function showbyid($id)
    {
        $assignments = Assignment::find($id);
        return response()->json($assignments);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function edit(Assignment $assignment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $assignments = Assignment::find($id);
        $assignments->name = $request->input('name');
        $assignments->item = $request->input('item');
        $assignments->duedate = $request->input('duedate');
        $assignments->save();
        return response()->json($assignments);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $assignments = Assignment::find($id);
        $assignments->delete();
        return response()->json($assignments);
    }
}
