<?php

namespace App\Http\Controllers;

use App\Quiz;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $quizzes = new Quiz();
        $quizzes->name = $request->input('name');
        $quizzes->item = $request->input('item');
        $quizzes->duedate = $request->input('duedate');
        $quizzes->save();
        return response()->json($quizzes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $quizzes = Quiz::all();
        return response()->json($quizzes);
    }

    public function showbyid($id)
    {
        $quizzes = Quiz::find($id);
        return response()->json($quizzes);
    }

    public function showbylessonid($lesson_id)
    {
        $quizzes = Quiz::where('lesson_id','=',$lesson_id)->get();
        return response()->json($quizzes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function edit(Quiz $quiz)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quizzes = Quiz::find($id);
        $quizzes->name = $request->input('name');
        $quizzes->item = $request->input('item');
        $quizzes->duedate = $request->input('duedate');
        $quizzes->save();
        return response()->json($quizzes);
    }
    public function updatebylessonid(Request $request, $lesson_id)
    {
        // Removing first all quiz and replacing with new batch of quizzes.
        Quiz::where('lesson_id','=',$lesson_id)->delete();

        Quiz::insert($request->all());
        return response()->json([
            'status' => 'success'
        ]);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $quizzes = Quiz::find($id);
        $quizzes->delete();
        return response()->json($quizzes);
    }
}
