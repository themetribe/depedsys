<?php

namespace App\Http\Controllers;

use App\Student;
use App\User;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    private $defaultWith;

    public function __construct() {
        $this->defaultWith = [
            'User', 
            'Section',
            'Level',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)
    {
        # 
        #Required : 
        #$request->level_id 
        #$request->section_id
        #

        $user = new User();
        $user->fname = $request->fname;
        $user->mname = $request->mname;
        $user->lname = $request->lname;
        $user->contact = $request->contact;
        $user->address = $request->address;
        $user->gender = $request->gender;
        $user->bdate = $request->bdate;
        $user->password = bcrypt('letmein');
        $user->save();

        $student = new Student();
        $student->uid = $user->id;
        $student->level_id = $request->level_id;
        $student->section_id = $request->section_id;
        $student->save();

        return response()->json([
            "message" => "Student record created"
        ], 201);

    }


    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $students = Student::with($this->defaultWith)->get();
        return response()->json($students);
    }

    public function showbysectionid($id)
    {
        $students = Student::where('section_id', $id)->with($this->defaultWith)->get();
        return response()->json($students);
    }

    public function showbyid($id)
    {
        $students = Student::find($id);
        return response()->json($students);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);
        
        $newData = $request->all();
        $isUserChanged = false;
        $user = User::find($student->user_id);
        foreach($newData as $key=>$val) {
            switch($key) {
                case "level":{
                    $student->level_id = $val['value'];
                    break;
                }
                case "section":{
                    $student->section_id = $val['value'];
                    break;
                }
                case "fname":
                case "lname":
                case "gender": {
                    $user->{$key} = $val;
                    $isUserChanged = true;
                    break;
                }
                default : 
                    $student->{$key} = $val;
            }
        }
        if($isUserChanged) {
            $user->save();
        }
        $student->save();
        return response()->json([
            "status" => "success",
            "newData" => $newData
        ], 201);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $students = Student::find($id);
        $students->delete();
        return response()->json($students);
    }
}
