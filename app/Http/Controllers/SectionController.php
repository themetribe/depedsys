<?php

namespace App\Http\Controllers;

use App\Section;
use App\Level;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    private $defaultWith;

    public function __construct() {
        $this->defaultWith = [
            'Level',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "hi I'm index";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $sections = new Section();
        $sections->name = $request->name;
        $sections->level_id = $request->level_id;
        $sections->save();

    return response()->json([
        "message" => "Section record created"
    ], 201);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $sections = Section::with($this->defaultWith)->get();
        return response()->json($sections);
    }

    public function showbyid($id)
    {
        $sections = Section::find($id);
        return response()->json($sections);
    }
    public function showbylevelid($id)
    {
        $sections = Section::where('level_id', $id)->with($this->defaultWith)->get();
        return response()->json($sections);
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sections = Section::find($id);
        $newData = $request->all();
        foreach($newData as $key=>$val) {
            $sections->{$key} = $val;
        }
        $sections->save();
        return response()->json([
            "status" => "success",
            "newData" => $newData
        ], 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $sections = Section::find($id);
        $sections->delete();
        return response()->json($sections);
    }
}
