<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Assignment;
use Illuminate\Http\Request;

class LessonController extends Controller
{

    private $defaultWith;


    public function __construct() {
        $this->defaultWith = [
            'Assignments' => function($query) {
                $query->select('id', 'description');
            }
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $lessons = new Lesson();
        $lessons->name = $request->name;
        $lessons->date = $request->date;
        $lessons->subject_id = $request->subject_id;
        $lessons->save();

    return response()->json([
        "message" => "Lesson record created"
    ], 201);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $lessons = Lesson::with(['Subject'])->get();
        return response()->json($lessons);
    }

    public function showbyid($id)
    {
        $lessons = Lesson::with('Assignments')->find($id);
        return response()->json($lessons);
    }
    public function showbysubjectid($id)
    {
        $lessons = Lesson::where('subject_id', $id)->with(['Subject'])->get();
        return response()->json($lessons);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function edit(Lesson $lesson)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Lesson::where('id', $id)->exists()) {
            
            $lesson = Lesson::find($id);
            $newData = $request->all();

            foreach($newData as $key=>$val) {
                switch($key) {
                    case "subject":{
                        $lesson->subject_id = $val['value'];
                        break;
                    }
                    case "assignments": {
                        /*Pseudo : 
                        1 count assignments
                        2. loop assignemnts
                        3. check if assignment id exist, then : 
                            update data
                        4. else : 
                            save new data
                        5. clear assignments for this lesson that is not on the passed array
                        */

                        $assignment_ids = [];
                        if(count($request->assignments) > 0) {
                            foreach($request->assignments as $key=>$val) {
                                if(Assignment::where('id', $val['id'])->exists()) {
                                    array_push($assignment_ids, $val['id']);
                                    $assignment = Assignment::find($val['id']);
                                    $assignment->description = $val['description'];
                                    $assignment->save();
                                }
                                else {
                                    $assignment = new Assignment();
                                    $assignment->description = $val['description'];
                                    $assignment->lesson_id = $id;
                                    $assignment->save();
                                    array_push($assignment_ids, $assignment->id);
                                }
                            }
                            Assignment::whereNotIn('id', $assignment_ids)->delete();
                        }
                        break;
                    }
                    default : 
                        $lesson->{$key} = $val;
                }
            }
            $lesson->save();
            
            return response()->json([
                "message" => "Lesson record updated!"
            ], 201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $lessons = Lesson::find($id);
        $lessons->delete();
        return response()->json($lessons);
    }
}
