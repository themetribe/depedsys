<?php

namespace App\Http\Controllers;

use App\User; 
use App\Student;

use Illuminate\Http\Request;
class UserController extends Controller
{

    public function __construct() {}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){}

    /**
     * Display the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show(){}

    public function showbyid($id){ 
        $data = User::find($id);
        $student = Student::where('user_id', $id)->with(['Section', 'Level'])->first();
        if($student) {
            $data->level = $student->level;
            $data->section = $student->section;
        }
        return response()->json($data);
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $user = User::find($id);
        $student = Student::where('user_id', $id)->first();
        $newData = $request->all();
        $isStudentChanged = false;
        foreach($newData as $key=>$val) {
            switch($key) {
                case "level":{
                    $student->level_id = $val['value'];
                    $isStudentChanged = true;
                    break;
                }
                case "section":{
                    $student->section_id = $val['value'];
                    $isStudentChanged = true;
                    break;
                }
                default : 
                    $user->{$key} = $val;
            }
            
        }
        if($isStudentChanged) {
            $student->save();
        }
        $user->save();
        return response()->json([
            "status" => "success",
            "newData" => $newData
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id){
        $data = User::find($id);
        $data->delete();
        return response()->json($data);
    }


}