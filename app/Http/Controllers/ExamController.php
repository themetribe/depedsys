<?php

namespace App\Http\Controllers;

use App\Exam;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $exams = new Exam();
        $exams->name = $request->input('name');
        $exams->item = $request->input('item');
        $exams->duedate = $request->input('duedate');
        $exams->save();
        return response()->json($exams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $exams = Exam::all();
        return response()->json($exams);
    }

    public function showbyid($id)
    {
        $exams = Exam::find($id);
        return response()->json($exams);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exams = Exam::find($id);
        $exams->name = $request->input('name');
        $exams->item = $request->input('item');
        $exams->duedate = $request->input('duedate');
        $exams->save();
        return response()->json($exams);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $exams = Exam::find($id);
        $exams->delete();
        return response()->json($exams);
    }
}
