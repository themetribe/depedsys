<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    private $defaultWith;

    public function __construct() {
        $this->defaultWith = [
            'Level',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "hi I'm index";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $subjects = new Subject();
        $subjects->name = $request->name;
        $subjects->level_id = $request->level_id;
        $subjects->save();

    return response()->json([
        "message" => "Subject record created"
    ], 201);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $subjects = Subject::with($this->defaultWith)->get();
        return response()->json($subjects);
    }

    public function showbyid($id)
    {
        $subjects = Subject::find($id)->with($this->defaultWith);
        return response()->json($subjects);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subjects = Subject::find($id);
        $newData = $request->all();
        foreach($newData as $key=>$val) {
            switch($key) {
                case "level":{
                    $subjects->level_id = $val['value'];
                    break;
                }
                default : 
                    $subjects->{$key} = $val;
            }
        }
        $subjects->save();
        return response()->json([
            "status" => "success",
            "newData" => $newData
        ], 201);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $subjects = Subject::find($id);
        $subjects->delete();
        return response()->json($subjects);
    }
}
