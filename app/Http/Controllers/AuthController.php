<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\User as User;
use App\User as AppUser;
use App\Http\Controllers\Auth as Auth;
use Carbon\Carbon;
use Dotenv\Loader\Parser;

class AuthController extends Controller
{
  

    // public function __construct() 
    // {
        // $this->middleware( 'auth:api', ['except'=>['login']]);
    // }
    public function register(Request $request) {
        $validateData = $request->validate([
            'fname' => ['required', 'string', 'max:255'],
            'mname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'school' => ['required', 'string', 'max:255'],
            'contactnumber' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string', 'max:10'],
            'email' => 'required|email|unique:users', 
            'password' => 'required', 
        ]);

            $validateData['password'] = bcrypt($request->password);

            $user = AppUser::create($validateData);
            $token = auth()->login($user);
            return $this->respondWithToken($token, $user);
    }

    public function update(Request $request, $id)
    {
        
         $users = AppUser::find($id);
         $newData = $request->all();
        foreach($newData as $key=>$val) {
            $users->{$key} = $val;
        }
        $users->save();
        return response()->json([
            "status" => "success",
            "newData" => $newData
        ], 201);
 
    }

    public function login(Request $request) {
        // echo "hello"; exit();
        // return "hello";
        // return 
        $credentials = request(['email', 'password']);

        if(!$token = auth()->attempt($credentials)) {
            return response()->json([
                'error' => 'Invalid email or password'], 401);
        }
        // return print_r($request->email, true);
        // exit();
        $user = AppUser::where('email', $request->email)->first();

        // $user = response()->json([
        //     'fname' => $user->fname,
        //     'lname' => $user->lname
        // ]);
        
        // $user = [
        //     'fname' => $user
        // ] = $_user['']
        // return print_r($user, true);
        return $this->respondWithToken($token, [
            'fname' => $user->fname,
            'lname' => $user->lname,
        ]);



    //     $loginData = $request->validate([
    //         'email' => 'email|required',
    //         'password' => 'required'
    //     ]);

    //    if ( !auth()->attempt($loginData)){
    //         return response(['error' => 'Invalid Credentials']);
    //    }

    //    $user = $request->user();
    //    $tokenRes = $user->createToken('Personal Access Token');
    //    $token = $tokenRes->token;

    //    if ($request->remember_me)
    //         $token->expires_at = Carbon::now()->addWeeks(1);
    //         $token->save();

    //         return response()->json([
    //             'message' => 'Success!',
    //             'access_token' =>   $tokenRes->accessToken,
    //             'token_type' =>     'Bearer',
    //             'expires_at' =>     Carbon::parse($tokenRes->token->expires_at)->toDateTimeString()
    //         ]);

            
    }

    private function respondWithToken($token, $user = null) {
        $return = [
            'token' => $token,
            'access_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ];
        if($user) {
            $return['user'] = $user;
        }
        return response()->json($return);
    }

    
    public function logout(Request $request) {

        auth()->logout();
        return response()->json(['msg' => 'User successfully logged out']);
        // $request->user()->token()->revoke();
        // return response()->json([
        //     'message' => 'Successfully logged out'
        // ]);
    }

    public function refresh() {
        return $this->respondWithToken(auth()->refresh());
    }

    public function me() {
        return response()->json(auth()->user());
    }

    // public function user(Request $request){
    //     return response()->json($request->user());
    // }
    
}
