<?php

namespace App\Http\Controllers;

use App\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "hi I'm index";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create(Request $request)
    {
       
        $levels = new Level();
        $levels->name = $request->name;
        $levels->img = $request->img;
        $levels->save();

    return response()->json([
        "message" => "Level/Grade record created"
    ], 201);

        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Level  $level
     * @return \Illuminate\Http\Response
     */

    public function show()
    {
        $levels = Level::all();
        return response()->json($levels);
    }

    public function showbyid($id)
    {
        $levels = Level::find($id);
        return response()->json($levels);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Level  $level
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $level = Level::find($id);
        $newData = $request->all();
        foreach($newData as $key=>$val) {
            $level->{$key} = $val;
        }
        $level->save();
        return response()->json([
            "status" => "success",
            "newData" => $newData
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $levels = Level::find($id);
        $levels->delete();
        return response()->json($levels);
    }
}
