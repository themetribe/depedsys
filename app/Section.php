<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = 'sections';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $fillable = ['name', 'level_id'];

    public function students() {
        return $this->belongsTo('App\Student', 'section_id', 'id');
    }

    public function level() {
        return $this->belongsTo('App\Level', 'level_id', 'id');
    }
    
}
