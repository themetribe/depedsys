<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $table = 'lessons';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];

    public function subject(){
        return $this->belongsTo('App\Subject', 'subject_id', 'id');
    }

    // Referenced from
    public function assignments() {
        return $this->hasMany('App\Assignment');
    }
}
