<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $table = 'resources';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];

    public function users(){
        return $this->belongsTo('App\User');
    }
    
    public function students(){
        return $this->belongsTo('App\Section');
    }

    public function levels(){
        return $this->belongsTo('App\Level');
    }

    public function subjects(){
        return $this->hasMany('App\Subject');
    }

}
