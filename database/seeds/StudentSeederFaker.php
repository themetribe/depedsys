<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentSeederFaker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Student');

        for ($i = 1; $i <= 5; $i++) {

            $newId = DB::table('users')->insertGetId([
                'fname' => $faker->firstName,
                'mname' => $faker->lastName,
                'lname' => $faker->lastName,
                'contact' => $faker->e164PhoneNumber,
                'address' => $faker->address,
                'gender' => $faker->randomElement(['Male', 'Female']),
                'bdate' => $faker->dateTime,
                'email' => $faker->email,
                'type' => 'Student',
                'password' => bcrypt('letmein'),
                'updated_at' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
            ]);

            DB::table('students')->insert([
                'user_id' => $newId,
                'level_id'=>rand(1,2),
                'section_id'=>rand(1,2),
            ]);

        }

    }
}
