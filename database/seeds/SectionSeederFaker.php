<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectionSeederFaker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Section');

        for ($i = 1; $i <= 10; $i++) {

            DB::table('sections')->insert([
                'name' => $faker->colorName,
                'level_id' => rand(1, 6),
                'updated_at' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
            ]);

        }
    }
}
