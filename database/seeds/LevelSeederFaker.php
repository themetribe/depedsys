<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
class LevelSeederFaker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Level');

       for($i = 1; $i <= 6; $i ++){

        
            DB::table('levels')->insert([
                'name' =>       "Grade " . $i,
                'img' => "grade".$i."bg.jpg",
                'updated_at' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
            ]);

       }
    }
}
