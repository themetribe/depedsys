<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Subject;

class SubjectSeederFaker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create(Subject::class);

        for ($i = 1; $i <= 5; $i++) {

            DB::table('subjects')->insert([
                'name' => $faker->firstName,
                'level_id' => rand(1, 6),
                'updated_at' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
            ]);

        }

    }
}
