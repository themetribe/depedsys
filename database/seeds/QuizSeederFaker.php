<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuizSeederFaker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Lesson');
        $choices = 'A|B|C|D';
        for ($i = 1; $i <= 5; $i++) {

            DB::table('quizzes')->insert([
                'description' => $faker->sentence,
                'lesson_id' => 1,
                'choices' => $choices,
                'answer' => ['A','B','C','D'][rand(0,3)],
                'duedate' => $faker->dateTimeBetween('now', '+2 days'),
                'updated_at' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
            ]);

        }

    }
}
