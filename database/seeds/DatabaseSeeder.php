<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StudentSeederFaker::class);
        $this->call(LevelSeederFaker::class);
        $this->call(SectionSeederFaker::class);
        $this->call(LessonSeederFaker::class);
        $this->call(SubjectSeederFaker::class);
        $this->call(QuizSeederFaker::class);
        $this->call(UserSeederFaker::class);
    }
}
