<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LessonSeederFaker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Lesson');

        for ($i = 1; $i <= 5; $i++) {

            DB::table('lessons')->insert([
                'title' => $faker->name,
                'content' => $faker->randomHtml(2, 3),
                'meetingUrl' => $faker->url,
                'subject_id' => rand(0, 5),
                'updated_at' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
            ]);

        }

    }
}
