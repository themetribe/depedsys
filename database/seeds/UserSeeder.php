<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UserSeederFaker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create(User::class);

        for ($i = 1; $i <= 5; $i++) {

            DB::table('users')->insert([
                'fname' => $faker->firstName,
                'mname' => $faker->lastName,
                'lname' => $faker->lastName,
                'contact' => $faker->e164PhoneNumber,
                'address' => $faker->address,
                'gender' => $faker->randomElement(['Male', 'Female']),
                'bdate' => $faker->dateTime,
                'email' => $faker->email,
                'type' => 'Teacher',
                'password' => bcrypt('letmein'),
                'updated_at' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
            ]);

        }

    }
}
