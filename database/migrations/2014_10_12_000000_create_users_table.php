<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('fname');
            $table->string('mname')->nullable();
            $table->string('lname');
            $table->enum('gender', ['Male', 'Female']);
            $table->date('bdate')->nullable();
            $table->string('email')->nullable();
            $table->string('password');
            $table->string('address')->nullable();
            $table->string('contact')->nullable();
            $table->string('avatar')->nullable();
            $table->enum('type', ['Student', 'Teacher', 'Principal', 'Supervisor']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
